# Blockly Physique

Création de blocs physiques pour la logique de programmation pour les enfants et les grands enfants !

Liste des blocs à faire par ordre de priorités + nombres de blocs à faire pour chaque style :

1. Démarrer 
    * Quantité : 1
    * La voiture commence à faire le programme une fois que le bouton a été pressé

2. Fin 
    * Quantité : 1
    * La voiture s'arrête là où elle est

2. Déplacer vers la droite 
    * Quantité : 10
    * Avancer de 1 tour de roue vers la droite

2. Déplacer vers la gauche 
    * Quantité : 10
    * Avancer de 1 tour de roue vers la gauche
2. Tourner à droite 
    * Quantité : 10
    * Tourner de un nombre de degré (potentiomètre tourné avec un visuel pour savoir où on va) vers la droite en avançant ?
2. Tourner à gauche 
    * Quantité : 10
    * Tourner de un nombre de degré (potentiomètre tourné avec un visuel pour savoir où on va) vers la gauche en avançant ?
2. Répéter 
    * Quantité : 3 
    * Boucle qui repète un nombre de fois saisi par l'utilisateur ce qui est dedans
2. Répéter indéfiniment 
    * Quantité : 3
    * Boucle qui répète indéfiniment ce qui est dedans
2. Arrêter le perso 
    * Quantité : 3
    * La voiture s'arrête là où elle est
2. Fixer la vitesse (3 positions) 
    * Quantité : 5
    * L'utilisateur a le choix entre 3 vitesses (faible, moyenne, forte) pour faire avancer la voiture
2. Retourner à la position de départ 
    * Quantité : 3
    * La voiture revient à la position de départ
2. Attendre 
    * Quantité : 3
    * La voiture s'arrête puis reprend après ... secondes
2. Klaxonne 
    * Quantité : 3
    * La voiture klaxonne (=émet un son)
2. Clignotant 
    * Quantité : 3
    * La voiture met les clignotants lorsqu'elle tourne à gauche ou à droite (=gestion de LED)
2. Condition ?