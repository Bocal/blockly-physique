/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// Pin 11 has the LED on Teensy 2.0
// Pin 6  has the LED on Teensy++ 2.0
// Pin 13 has the LED on Teensy 3.0
// give it a name:
int led = 13;
int led_1=2;
int led_2=3;
int led_3=4;
int led_4=5;
int led_5=6;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
}

// the loop routine runs over and over again forever:

void loop() {
  int sensorValue = analogRead(A0);
  
  digitalWrite(led_1, HIGH);
  
  
    if (sensorValue >1023/5 && sensorValue <= 2*1023/5){
      digitalWrite (led_2, HIGH);    
    }
    else{
      digitalWrite (led_2, LOW);
      digitalWrite (led_3, LOW);
      digitalWrite (led_4, LOW);
      digitalWrite (led_5, LOW);
      if (sensorValue > 2*1023/5 && sensorValue <= 3*1023/5){
        digitalWrite (led_3, HIGH);
        digitalWrite (led_2, HIGH);
      }
      else{
        digitalWrite (led_3, LOW);
        digitalWrite (led_4, LOW);
        digitalWrite (led_5, LOW);
        if (sensorValue >3*1023/5 && sensorValue <= 4*1023/5){
          digitalWrite (led_4, HIGH);
          digitalWrite (led_3, HIGH);
          digitalWrite (led_2, HIGH);
        }
        else{
          digitalWrite (led_4, LOW);
          digitalWrite (led_5, LOW);
          if (sensorValue >4*1023/5){
            digitalWrite (led_5, HIGH);
            digitalWrite (led_4, HIGH);
            digitalWrite (led_3, HIGH);
            digitalWrite (led_2, HIGH);
          }
          else{
            digitalWrite (led_5, LOW);
          }
        }
      }
    }
}

